
## Installation
1、先在外部根目录下 通过package.json 安装依赖
2、再进入docc目录，再通过 他下面的 package.json 安装子依赖


### 命令行

```
dev   //可以启动本地的开发
build:bundle   //编译组件功能
build:type   //自动生成 .d.ts文件
build   // 编译组件功能  并  自动生成 .d.ts文件
lint   //检测语法错误
```

package.json的字段
```
engines  // 对nodejs 和 npm版本的限制
main    //commonjs 模块下加载的包，  通过require时，引入此处的文件
module  // 在Es模块下加载的包， 通过 import时引入此处的文件
types    //typescipt校验的 d.ts的文件路径
unpkg    //通过 cdn 访问
```


https://github.com/dhershman1/vue-debounce
https://github.com/scholtz/qrcode-vue3
