import process from 'node:process';
import { fileURLToPath, URL } from 'node:url';
import vue from 'rollup-plugin-vue';
import commonJs from '@rollup/plugin-commonjs'; //将CommonJS模块转换为ES6，以便在rollup打包时包含进去,因为rollup默认支持esm，不支持 cjs
import resolve from '@rollup/plugin-node-resolve'; //打包第三方库
import replace from '@rollup/plugin-replace'; //在打包时用来替换变量名的值
import images from '@rollup/plugin-image';
import postcss from 'rollup-plugin-postcss';
import alias from '@rollup/plugin-alias';
import copy from 'rollup-plugin-copy';
import autoprefixer from 'autoprefixer'; //自动添加css前缀
import esbuild from 'rollup-plugin-esbuild'; //压缩js代码  这三个插件的替代品 rollup-plugin-typescript2, @rollup/plugin-typescript 和 rollup-plugin-terser
import Case from 'case';
import packageJson from '../package.json';

const libraryName = packageJson.name;
const entryPath = 'src/index.ts';
const banner = `/*!
* ${libraryName} ${packageJson.version}
*
* @author ${packageJson.author}
* @link ${packageJson.homepage}
* @license ${packageJson.license}
*/`; //顶部插入文本

const rootSrc = fileURLToPath(new URL('../src', import.meta.url));

const plugins = [
  vue(),
  postcss({
    minimize: true,
    plugins: [autoprefixer()],
    extract: false,
  }),
  images(),
  alias({
    entries: [{ find: '@', replacement: rootSrc }],
  }),
  replace({
    preventAssignment: true,
    'process.env.NODE_ENV': JSON.stringify('production'),
  }),
  resolve({
    extensions: ['.js', '.ts', '.vue'],
    browser: true,
  }),
  commonJs(),
  esbuild({
    exclude: /node_modules/, // default
    sourceMap: true, // default
    minify: process.env.NODE_ENV === 'production',
    target: 'esnext', // default, or 'es20XX', 'esnext'
    tsconfig: 'tsconfig.json', // default
    // Add extra loaders
  }),
];

export default [
  {
    input: entryPath,
    output: {
      file: `dist/${libraryName}.esm.min.js`,
      format: 'esm',
      banner,
    },
    plugins,
    external: ['vue'], //将项目中引入的 vue模块在打包时排除掉。这样项目中原封不动把这个模块打包进入，但是要在使用本库的项目中引入 这个模块，否则报错
  },
  {
    input: entryPath,
    output: {
      file: `dist/${libraryName}.cjs.min.js`,
      format: 'cjs',
      banner,
    },
    plugins,
    external: ['vue'],
  },
  {
    input: entryPath,
    output: {
      file: `dist/${libraryName}.iife.min.js`,
      format: 'iife',
      name: Case.pascal(libraryName), //通过 window[libraryName] 访问本模块库
      banner,
    },
    plugins,
    external: ['vue'],
  },
  {
    input: entryPath,
    output: {
      file: `dist/${libraryName}.umd.min.js`,
      format: 'umd',
      name: Case.pascal(libraryName), //通过 window[libraryName] 访问 访问本模块库
      globals: { vue: 'Vue' }, //通常配合下面external一起，被external排除后，就要通过window.Vue引入依赖
      banner,
    },
    plugins,
    external: ['vue'],
  },
];
