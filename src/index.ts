import './styles/index.scss';
import type { App } from 'vue';
import VirtualCanvasTree from '@/components/virtual-canvas-tree.vue';

const components = VirtualCanvasTree; //在项目中通过 import 直接引用组件
components.install = (app: App) => {
  //全局注册后，在项目中，不用import也能用
  app.component(VirtualCanvasTree.name, VirtualCanvasTree);
};
export default components;
